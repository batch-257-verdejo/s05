from django.urls import path
# from . import views
from todolist.views import index, todoitem, eventitem, register, register_user, update_user, change_password, login_view, logout_view, add_task, update_task, delete_task, add_event, update_event, delete_event


# Adding a namespace to this urls.py help django distinguish this set of routes from other urls.py files in other packages
app_name = 'todolist'
urlpatterns = [
    path('', index, name = 'index'),

    # localhost:8000/todolist/1
    path('task/<int:todoitem_id>/', todoitem, name = 'viewtodoitem'),

    path('event/<int:eventitem_id>/', eventitem, name = 'vieweventitem'),
    
    path('register/', register, name = "register"),

    path('register_user/', register_user, name = "register_user"),

    path('update_user/', update_user, name = "update_user"),

    path('change-password/', change_password, name = "change_password"),

    path('login/', login_view, name = "login"),

    path('logout/', logout_view, name = "logout"),

    # Task paths
    path('add_task/', add_task, name = "add_task"),

    path('<int:todoitem_id>/edit/', update_task, name = "update_task"),

    path('<int:todoitem_id>/', delete_task, name = "delete_task"),

    # Event paths
    path('add_event/', add_event, name = "add_event"),

    path('event/<int:eventitem_id>/edit/', update_event, name = "update_event"),

    path('event/<int:eventitem_id>/', delete_event, name = "delete_event")

]

















# urlpatterns = [
#     path('', views.index, name = 'index'),

#     # localhost:8000/todolist/1
#     path('<int:todoitem_id>/', views.todoitem, name = 'viewtodoitem'),
    
#     path('register/', views.register, name = "register"),

#     path('change-password/', views.change_password, name = "change_password"),

#     path('login/', views.login_view, name = "login"),

#     path('logout/', views.logout_view, name = "logout")

# ]
from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(label="Username", max_length=20)
    password = forms.CharField(label="Password", max_length=20)

class AddTaskForm(forms.Form):
    task_name = forms.CharField(label="Task Name", max_length=50)
    description = forms.CharField(label="Description", max_length=200)

class UpdateTaskForm(forms.Form):
    task_name = forms.CharField(label = "Task Name", max_length = 50)
    description = forms.CharField(label = "Description", max_length = 200)
    status = forms.CharField(label = "Status", max_length = 50)

# Event Forms
class AddEventForm(forms.Form):
    event_name = forms.CharField(label="Event Name", max_length=50)
    description = forms.CharField(label="Description", max_length=200)
    event_date = forms.DateField(label="Event Date")

class UpdateEventForm(forms.Form):
    event_name = forms.CharField(label = "Task Name", max_length = 50)
    description = forms.CharField(label = "Description", max_length = 200)
    event_date = forms.DateField(label="Event Date")
    status = forms.CharField(label = "Status", max_length = 50)

class RegisterForm(forms.Form):
    first_name = forms.CharField(label = "First Name", max_length = 100, required = True)
    last_name = forms.CharField(label = "Last Name", max_length = 100, required = True)
    username = forms.CharField(label = "Username", max_length = 50, required = True)
    email = forms.CharField(label = "email", max_length = 100, required = True)
    password = forms.CharField(label = "Password", max_length = 50, required = True)
    

class UpdateUserForm(forms.Form):
    first_name = forms.CharField(label = "First Name", max_length=50)
    last_name = forms.CharField(label = "Last Name", max_length=50)
    password = forms.CharField(label = "Password", max_length=50, required = False)
    new_password = forms.CharField(label="New Password", max_length=50, required=False)
    
